<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-com-duckduckgo-spice-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiComDuckduckgoSpice;

use Stringable;

/**
 * ApiComDuckduckgoSpiceDestinationInterface interface file.
 * 
 * This represents the destination conversions.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74InterfaceMetadata
 * 
 * @author Anastaszor
 */
interface ApiComDuckduckgoSpiceDestinationInterface extends Stringable
{
	
	/**
	 * Gets the destination currency code.
	 * 
	 * @return string
	 */
	public function getQuotecurrency() : string;
	
	/**
	 * Gets the ratio.
	 * 
	 * @return float
	 */
	public function getMid() : float;
	
}
