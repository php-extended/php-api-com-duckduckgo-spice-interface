<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-com-duckduckgo-spice-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiComDuckduckgoSpice;

use DateTimeInterface;
use Psr\Http\Message\UriInterface;
use Stringable;

/**
 * ApiComDuckduckgoSpiceResponseInterface interface file.
 * 
 * This class is a simple implementation of the
 * ApiComDuckduckgoSpiceResponseInterface.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74InterfaceMetadata
 * 
 * @author Anastaszor
 */
interface ApiComDuckduckgoSpiceResponseInterface extends Stringable
{
	
	/**
	 * Gets the link to the legal terms of use.
	 * 
	 * @return UriInterface
	 */
	public function getTerms() : UriInterface;
	
	/**
	 * Gets the link to the privacy policy.
	 * 
	 * @return UriInterface
	 */
	public function getPrivacy() : UriInterface;
	
	/**
	 * Gets the source currency code.
	 * 
	 * @return string
	 */
	public function getFrom() : string;
	
	/**
	 * Gets the initial amount.
	 * 
	 * @return float
	 */
	public function getAmount() : float;
	
	/**
	 * Gets the timestamp of the conversion.
	 * 
	 * @return DateTimeInterface
	 */
	public function getTimestamp() : DateTimeInterface;
	
	/**
	 * Gets the conversions.
	 * 
	 * @return array<int, ApiComDuckduckgoSpiceDestinationInterface>
	 */
	public function getTo() : array;
	
}
