<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-com-duckduckgo-spice-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiComDuckduckgoSpice;

use PhpExtended\DataProvider\UnprovidableThrowable;
use PhpExtended\Reifier\ReificationThrowable;
use Psr\Http\Client\ClientExceptionInterface;
use Stringable;

/**
 * ApiComDuckduckgoSpiceEndpointInterface interface file.
 * 
 * This interface represents the spice api of the duckduckgo.com website.
 * 
 * @author Anastaszor
 */
interface ApiComDuckduckgoSpiceEndpointInterface extends Stringable
{
	
	/**
	 * Gets the rate data from the spice API for the given currency.
	 * 
	 * @param string $source the source currency code
	 * @param array<integer, string> $dests the destination currency codes
	 * @return ApiComDuckduckgoSpiceResponseInterface
	 * @throws ClientExceptionInterface
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getRates(string $source, array $dests) : ApiComDuckduckgoSpiceResponseInterface;
	
}
